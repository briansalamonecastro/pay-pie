"use client";

import { Button, Checkbox, Grid, Table } from "@mantine/core";

export default function HomePage() {
  const debts = [
    { id: 1, creditor: "Lucas", debtor: "Tony", amount: 16.66, paid: false },
    { id: 2, creditor: "Lucas", debtor: "Katy", amount: 6.66, paid: false },
  ];

  const rows = debts.map((debt) => (
    <Table.Tr key={debt.id}>
      <Table.Td>
        <b>{debt.debtor}</b> owes <b>{debt.creditor}</b>
        <br />
        <b>${debt.amount}</b>
      </Table.Td>
      <Table.Td>
        <Checkbox checked={debt.paid} />
      </Table.Td>
    </Table.Tr>
  ));

  return (
    <main>
      <Grid>
        <Grid.Col span="content">
          <Button variant="outline">Food</Button>
        </Grid.Col>
        <Grid.Col span="content">
          <Button variant="outline">Drinks</Button>
        </Grid.Col>
        <Grid.Col span="content">
          <Button variant="outline">Rest</Button>
        </Grid.Col>
      </Grid>

      <Table>
        <Table.Thead>
          <Table.Tr>
            <Table.Th>Debts</Table.Th>
            <Table.Th>Paid</Table.Th>
          </Table.Tr>
        </Table.Thead>
        <Table.Tbody>{rows}</Table.Tbody>
      </Table>
    </main>
  );
}
